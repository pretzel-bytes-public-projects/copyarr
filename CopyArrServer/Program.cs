// ReSharper disable once CheckNamespace
namespace CopyArrServer;

internal static partial class Program
{
    [STAThread]
    private static void Main()
    {
        var builder = WebApplication.CreateBuilder();
        ConfigureProgram(builder);
        var app = builder.Build();
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        app.MapControllers();
        app.Run();
    }
    
}