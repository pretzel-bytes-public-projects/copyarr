﻿// Copyright (c) Pretzel Bytes LLC 2022-2024
using NLog;
using NLog.Extensions.Logging;

// ReSharper disable once CheckNamespace
namespace CopyArrServer;

internal static  partial class Program 
{
    private static bool ConfigureProgram(WebApplicationBuilder builder)
    {
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen();
        builder.Services.AddControllers();
        IConfigurationRoot config = new ConfigurationBuilder()
            .AddJsonFile(path: "AppSettings.json").Build();
        NLog.Extensions.Logging.ConfigSettingLayoutRenderer.DefaultConfiguration = config;
        
        var logger = LogManager
            .Setup()
            .GetCurrentClassLogger();
        logger.Info("Pretzel Bytes Authentication API Startup");
        builder.Logging.AddNLog();

        var connectionString = config.GetSection("Database").GetSection("ConnectionString").Value ?? "";
        var databaseName = config.GetSection("Database").GetSection("DatabaseName").Value ?? "";
        Console.WriteLine($"CS {connectionString}");
        Console.WriteLine($"DN {databaseName}");
     
       // builder.Services.AddScoped<LoginJwtCheck>();

        //builder.Services.AddSingleton<IGlobalVariableDataService, GlobalVariableDataService>();
        
       // builder.Services.AddControllers();

        return true;
    }

}